// ==UserScript==
// @name         Apple music RPD
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://music.apple.com/*
// @icon         https://www.google.com/s2/favicons?domain=tampermonkey.net
// @grant        GM_xmlhttpRequest
// ==/UserScript==


function scraper() {
    let data = document.querySelector("#apple-music-player")
    if (data) {
        let [title, artist, album] = data.title.split('-').map(x => x.trim())
        console.log(artist);
        send_info(title, artist, album);
    }
}

function send_info(title, artist, album) {
    const params = {
        title,
        artist,
        album
    };
    GM_xmlhttpRequest({
        method: "POST",
        url: "http://127.0.0.1:5000/api/itunes",
        data: JSON.stringify(params),
        headers: {
            "Content-Type": "application/json"
        },
        onload: function (response) {
            console.log(response.responseText)
        }
    });
}

(function () {
    'use strict';
    setInterval(scraper, 20000)
})();

