import os
from os.path import join, dirname

from dotenv import load_dotenv
from flask import Flask,request, render_template,jsonify

from RichPresence import RichPresence

# Configure program
app = Flask(__name__, template_folder="templates")
dotenv_path = join(dirname(__file__), ".env")
load_dotenv(dotenv_path)
client_id = os.getenv("CLIENT_ID")
richPresence = RichPresence(client_id)


@app.route("/api/itunes", methods=["POST"])
def result():
    data:dict = request.get_json()
    title, artist, album = data.values()
    print(title, artist, album)
    richPresence.update(title, artist, album)
    return jsonify(data)


@app.route("/test")
def test():
    richPresence.cpuExample()
    return render_template("cpu.html")


@app.route("/")
def hello_world():
    return "Hello, World!"


app.run()
