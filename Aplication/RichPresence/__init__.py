import asyncio

import psutil
from pypresence import Presence


def get_or_create_eventloop():
    try:
        return asyncio.get_event_loop()
    except RuntimeError as ex:
        if "There is no current event loop in thread" in str(ex):
            print("Creating event loop")
            loop = asyncio.new_event_loop()
            return loop


class RichPresence:
    RPC = None

    def __init__(self, client_id):
        # client_id = os.getenv("CLIENT_ID")  # Put your client ID here
        self.RPC = Presence(client_id, loop=get_or_create_eventloop())
        print(client_id)
        self.RPC.connect()

    # {"title": "Toi Et Moi", "artist": "Paradis", "album": "Recto Verso"}
    def update(self, title, artist, album):
        self.RPC.update(state=f"Escuchando {title}", details=f"{artist} - {album}")

    def cpuExample(self):
        cpu_per = round(psutil.cpu_percent(), 1)  # Get CPU Usage
        psutil.virtual_memory()
        mem_per = round(psutil.virtual_memory().percent, 1)
        toPrint = self.RPC.update(
            details="RAM: " + str(mem_per) + "%", state="CPU: " + str(cpu_per) + "%"
        )
        print(toPrint)  # Set the presence
